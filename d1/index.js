console.log("Hello World");

// Assignment Operator (=)
let assignmentNumber = 8;

// Arithmetic Operators
// + - * / %

// Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber);

// shorthand
assignmentNumber += 2;
console.log(assignmentNumber);

// Subtraction/Multiplication/Division assignment operator (-=, *=, /=)

assignmentNumber -= 2;
console.log(assignmentNumber);

assignmentNumber *= 2;
console.log(assignmentNumber);

assignmentNumber /= 2;
console.log(assignmentNumber);

// Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);


let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

// Increment and Decrement Operator

let z = 1;

// increment
// pre-fix incrementation.

++z,
console.log(z); //2 - the value of z was added with 1 and is immediately returned.

// post-fix incrementation
z++;
console.log(z); //3 - the value of z was added with 1.
console.log(z++);//3
console.log(z);//4

// pre-fix vs post-fix incrementation
console.log(z++);//4
console.log(z);//5

console.log(++z);//6

// pre-fix and post-fix decrementation
console.log(--z); //5

console.log(z--); //5
console.log(z); //4


// Type/Force Coercion
let numA = '10';
let numB = 12;

let coercion = numA = numB;
console.log(coercion); //1012
console.log(typeof coercion);


// sample 2

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;

console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE);

let numF = false + 1;
console.log(numF); //0

// Comparison Operators
let juan = 'juan';

// (=) assignment operator
/* (==) equality operator
	a.
*/

console.log(1 == 1);//true
console.log(1 == 2);//false
console.log(1 == '1');//true
console.log(0 == false);//true
console.log('juan' == 'JUAN');//false, case sensitive
console.log('juan' == juan);//true

// != Inequality Operator

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false);//false
console.log('juan' != 'JUAN');//true
console.log('juan' != juan);//false

// strictly equality Operator
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false = not the same data type
console.log(0 === false);//false = not the same data type
console.log('juan' === 'JUAN');//false
console.log('juan' === juan);//true

	
// strictly Inequality Operator
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false);//true
console.log('juan' !== 'JUAN');//true
console.log('juan' !== juan);//false


// Relational Comparison

let x = 500;
let y = 700;
let w = 8000;
let numString = '5500';

// Greater than (>)
console.log(x > y);//false
console.log(w > y);//true

//Less than (<)
console.log(w < x);//false
console.log(y < y);//false
console.log(x < 1000);//true
console.log(numString < 1000);//false = forced coercion
console.log(numString < 6000);//true = forced coercion (change string to number)
console.log(numString < "jose");//true = forced coercion (string to string)


						/*03-08-22*/

// Greater Than or Equal
console.log('March 08 2022');
console.log(w >= 8000);//true

// less Than or Equal to
console.log(x <= y);//true
console.log(y <= y);//true


// Logical Operators (&&),(||)
let isAdmin = false;
let isRegistered = true;
let isLegal = true;

				console.log('Logical AND(&&) Operators');
				// 1. Logical "AND" Operator (&& - Double Ampersand)
				// returns true if ALL operands are "TRUE".

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegal && isRegistered;
console.log(authorization2); //true


let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); //false

let authorization4 = isRegistered && isLegal && requiredLevel === 95;
console.log(authorization4); //true

let userName = 'gamer2022';
let userName2 = 'shadow1991';
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1); //false
// .length is 

let registration2 = userName2.length > 8 && userAge2 >=requiredAge
console.log(registration2); //true



				console.log('Logical OR(||) Operator');
				//Or Operator (|| - Double pipe)
				// returns true if atleast ONE of the operands are true.

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered && userLevel2 >= requiredLevel && 
userAge2 >=requiredAge;
console.log(guildRequirement1);//false

let guildRequirement3 = isRegistered || userLevel2 >= requiredLevel ||
userAge2 >= requiredAge;
console.log(guildRequirement3);//true


let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(guildRequirement2);//false



			console.log('Not Operator');
			// NOT Operator (!)
			// turns a boolean into the OPPOSITE value.

let guildAdmin = !isAdmin || userLevel2 >=requiredLevel
console.log(guildAdmin);//true

console.log(!isRegistered)//false
console.log(!isLegal)//false

let opposite = !isAdmin;
let opposite2 = !isLegal;

console.log(opposite);//true - isAdmin original value = false
console.log(opposite2);//false - isAdmin original value = true

		

			// IF, Else IF, and ELSE statement

// IF Statement
/*if statement will run a code block if the condition is true
or results is true*/

// if(true){
// 	alert('We just run an IF condition');
// }


let numG = -1;
if(numG < 0){
	console.log('Hello');
} 


let userName3 = 'Crusader_1993';
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
	console.log('Welcome to Game Online!');
}



if(userLevel3 >= requiredLevel){
	console.log('You are qualified to join the guild');
} else{
	console.log('You are not ready to be an admin')
}



if(userName3.length >= 10 && isRegistered && isAdmin){
	console.log('Thank You for joining the admin');//none - isAdmin is false
}

				//ELSE statement
/*the "ELSE" statement execute a block of codes if all other
conditions are false.*/

if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge){
	console.log('Thank you for joining the noobies guild')
} else{
	console.log('youre too strong to be a noob');
}


					// ELSE IF Statement
/*ELSE IF executes the statement if the previous or the original
condition is false or resulted to false but another specified condition
resulted to true.
*/

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log('Thank you for joining the noobies');
}	else if (userLevel > 25){
	console.log('you are too strong to be a noob.');
}	else if (userAge3 < requiredAge){
	console.log('youre to young to join the guild');

}	else {
	console.log('better luck next time.');
}		


			// if, else if and else statement with functions.
function addNum(num1, num2){
	if(typeof num1 === 'number' && typeof num2 === 'number'){
	console.log('run only if both arguments passed are number types')
	console.log(num1 + num2);
}	else {
	console.log('one or both of the arguments are not numbers');
}
}
addNum(5, '2');

let nunsample = 5;
console.log(typeof nunsample);




					//create log in function
function logIn(username, password){
	if(typeof username === 'string' && typeof password === 'string'){
		console.log('Both arguments are strings')

		}if(password.length >= 8 && username.length >= 8){
			console.log('Thank you for logging in')

 		}else if(username.length <= 8){
			console.log('Username too short')

 		}else if(password.length <= 8){
			console.log('Password too short')

 		}else if(password.length <= 8 && username.length <= 8){
			console.log('Credentials too short')

 		}else {
 		console.log('One of the arguments is not a string')
		}
	}
	
logIn('Jane', 'jane123');					
	



	
	

					//function with RETURN keyword
let message = 'No Message';
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'Not a typhoon yet.';
	} else if(windSpeed <= 61){
		return 'Tropical depression is detected.';
	} else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected.';
	} else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe Tropical storm detected.';
	} else{
		return 'Typhoon detected';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);

				//console.warn()
if(message == 'Tropical storm detected.'){
	console.warn(message);
}	


				// TRUTHY and FALSY
				//truthy
if(true){
	console.log('Truthy');
}

if(1){
	console.log('True');
}

if([]){
	console.log('Truthy')
}
	


				//falsy
				//-0, "", null, NaN
if(false){
	console.log('falsy');
}

if(0){
	console.log('False');
}

if(undefined){
	console.log('False');
}
	

				//Ternary Operator
/*
Syntax:
		-(expression/condition) ? = if (truthy), : = else (falsy)

		-Three operand of ternary operator:
		1. condition
		2. expression to execute if the condition is truthy
		3.expression to execute if the condition is false
*/

let ternaryResult = (1 < 18) ? true : false;
console.log(`Result of ternary operator is ${ternaryResult}`);

if(1 < 18){
	console.log(true)
} else{
	console.log(false)
}

let price = 5000;

price > 1000 ? console.log('price is over 1000') : console.log
('price is less than 1000');

//sample
let villain = 'harvey dent';
villain === 'two face'
? console.log('you lived long enough to be a villain')
: console.log('Not quite villainouse yet')

				/*ternary operators have an implicit "return" statement
				that without return keyword the resulting expression can be 
				stored in a variable.*/

						// else if ternary operator
let a = 7;
7 === 5
? console.log('A')
: (a === 10 ? console.log('A is 10') : console.log ('A is not 5 or 10'));

						// Multiple statement execution
// let name;

// function isOLegalAge(){
// 	name = 'john';
// 	return 'you are of the legal age limit';
// }

// function isOUnderAge(){
// 	name = 'jane';
// 	return 'you are under the age limit';
// }

// let age = parseInt(prompt('what is your age?'))
// console.log(age);

// 							// ? specifies the "if"
// 							// () specifies the condition
// 							// : specifies the "else"
// let legalAge = (age > 18) ? isOLegalAge() : isOUnderAge();
// console.log(`result of ternary operator in fucntions: ${legalAge}, ${name}`);




						// Mini Activity 04-09-22


	function tshirtColor1(day){
	if(day === 'monday'){
		alert(`Today is ${day}, Wear Black`)

		}else if(day === 'tuesday'){
			alert(`Today is ${day}, Wear Green`)

 		}else if(day === 'wednesday'){
			alert(`Today is ${day}, Wear Yellow`)

 		}else if(day === 'thursday'){
			alert(`Today is ${day}, Wear Red`)

 		}else if(day === 'friday'){
			alert(`Today is ${day}, Wear Violet`)

		}else if(day === 'saturday'){
			alert(`Today is ${day}, Wear Blue`)

 		}else if(day === 'sunday'){
			alert(`Today is ${day}, Wear White`)

 		}else {
 			alert('Enter a valid day of the week')
		}
	}

	
let day = prompt('Enter Day');
tshirtColor1(day)
 	
// parseInt() = convert a string to be a number


				// Switch statement
				/*
				Syntax:
					switch (expression/condition){
						case value:
							statement;
							break;
						default:
							statement;
							break:
					}
				*/


//sample

// let hero = prompt('type a hero').toLowerCase()   //prompt
let hero = 'jose rizal'  //ordinary

switch (hero) {
	case 'jose rizal':
		console.log('national hero of philippines');
		break;
	case 'george washington':
		console.log('hero of america revolution');
		break;
	case 'hercules':
		console.log('legendary hero of the greek');
		break;
	default:
		console.log('Please type again')


}

				// function sample (Switch Statement)
function roleChecker(role){
	switch (role){
		case 'admin':
			console.log('Welcome admin to the dashboard');
			break;
		case 'user':
			console.log('Your are not authorized to view this page');
			break;
		case 'guest':
			console.log('Go to the registration page to register');
			break;
		default:
			console.log('Invalid Role')	
			/*by default, your switch ends with default case,
			so therfore, even if there is no break keyword in your 
			default case, it will not run anything else*/
	}
}
roleChecker('admin');	


			// Try-Catch-Finally Statement

			/*
				this is used for error handling
			*/

function showIntensityAlert(windSpeed){
	// Attempt to execute a code
	try {
		alert(determineTyphoonIntensity(windSpeed))
	}catch (error){

		/*error or err are commonly used variable by developers for 
		storing errors*/

		console.log(typeof error);

		// Catch errors within 'try' statement
		/*the "error.message" is used to access the info 
		relating to an erro object*/

		console.warn(error.message);
	}
	finally {
		/*Continue execution of code REGARDLESS of success
		or failure of code execution in the 'try' block
		handle/resolve errors*/

		alert('Intensity updates will show new alert');
	}
}

showIntensityAlert(68);


						// Throw = user defined exception

						/*
						execution of the current function
						will stop.
						*/
// sample1

const number = 40;

try {
	if (number > 50){
		console.log('Success');
	}else {
		// user-deifned throw statement
		throw Error('The number is low');
	}
	// if throw executes, the below code does not execute
	console.log('hello')
}
	catch(error){
		console.log('An error caught');
		console.warn(error.message)
	}
	finally {
		console.log('Please add a higher number')
	}


// sample2

function getArea(width, height) {
	if(isNaN(width) || isNaN(height)) {
		throw 'Parameter is not a number';
	}
}
try{
	getArea(3, 'A');
}
catch(e) {
	console.error(e)
}
finally{
	alert('Number only');
}